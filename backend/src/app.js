const express = require('express');
const cors = require('cors');
const app = express();

app.use(cors());

const products = [
    {
        name: "Action figure Goku",
        price: "200,90",
        image: "./img/thumb_goku.jpg"
    },
    {
        name: "Action figure Samus",
        price: "120,80",
        image: "./img/thumb_samus.jpg"
    },
    {
        name: "Action figure Sub-zero",
        price: "200,90",
        image: "./img/thumb_subzero.jpg"
    },
    {
        name: "Action figure Link",
        price: "700,90",
        image: "./img/thumb_link.jpg"
    }
]

app.get('/products', (req, res) => {
    res.send(products);
});

app.listen(3001);