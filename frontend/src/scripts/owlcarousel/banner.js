$('.banner-slide').owlCarousel({
    loop: true,
    autoplay: true,
    nav: true,
    dots: false,
    navText: ['<img src="./svgs/angle-right-solid.svg">', '<img src="./svgs/angle-left-solid.svg">'],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});
