function initMenu() {
  const menuBtn = document.querySelector(".menu-btn");
  const dataDropdown = document.querySelector(".data-dropdown");
  menuBtn.addEventListener("click", handleClick);

  function handleClick() {
    dataDropdown.classList.toggle("active");
    outsideClick(this, ['click'], () => {
      dataDropdown.classList.toggle("active");
    });
  };
}

function outsideClick(element, events, callback) {
  const html = document.documentElement;
  const outside = 'data-outside';

  if (!element.hasAttribute(outside)) {
    events.forEach(userEvent => {
      html.addEventListener(userEvent, handleOutsideClick);
    })
    element.setAttribute(outside, '');
  }
  function handleOutsideClick(event) {
    if (!element.contains(event.target)) {
      element.removeAttribute(outside);
      events.forEach(userEvent => {
        html.removeEventListener(userEvent, handleOutsideClick);
      })
      callback();
    }
  }
}

initMenu();

function getApi() {
  const images = fetch(
    'http://localhost:3001/products'
  );
  images.then(products => {
    return products.json();
  }).then( data => {
      data.forEach(item => { 
        const img = document.createElement('img');
        const name = document.createElement('p');
        const price = document.createElement('h3')
        const box = document.querySelector('.card-store');
        const button = document.createElement('button');
        const sup = document.createElement('sup');
        const div = document.createElement('div');
        div.classList.add('cards');
        button.classList.add('buy-btn');
        button.innerText = "Comprar";
        img.src = item.image;
        name.innerText = item.name;
        price.innerText = item.price.substr(0,3)
        sup.innerText = item.price.substr(3,5);
        price.append(sup);
        div.append(img);
        div.append(name);
        div.append(price);
        div.append(button);
        box.append(div);
      });
    });
}


getApi();
